var app = angular.module('app',[]);
app.controller('CtrlA', ['$scope','$interval', 'TIMEZONE', function($scope, $interval, tz){

    $scope.moment = 1234;
    $scope.tz=tz;
    $interval(function(){
        $scope.moment = $scope.moment+1;
    },1000);
    console.log('$interval is mocked as function: ', typeof $interval.flush)
}]);
app.constant('TIMEZONE', 'Eastern');
app.filter('reverse', function() {
    return function(input, uppercase) {
      input = input || '';
      var out = "";
      for (var i = 0; i < input.length; i++) {
        out = input.charAt(i) + out;
      }
      // conditional based on optional argument
      if (uppercase) {
        out = out.toUpperCase();
      }
      return out;
    };
  });