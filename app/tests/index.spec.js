var $injector;
var $controller;
var $rootScope;
/*
 To debug run > node --debug-brk --no-lazy node_modules/jest/bin/jest.js
 and configure vscode to this port.
 node debug will block until vscode connect to the port. vscode debug point will work afterwards.
 ----vscode launch.json for debug ---
{
    "version": "0.2.0",
    "configurations": [
        {
            "type": "node",
            "request": "attach",
            "name": "Attach Node 5858",
            "port": 5858
        }
    ]
}
*/
test('jest should work itself', ()=>{
    expect(1+2017).toBe(2018);
});
describe('Angular environment ready', ()=>{
    it('angular is loaded and defined', ()=>{
        expect(angular).toBeTruthy();
    });
    it('angular mock is loaded and defined', ()=>{
        expect(angular.mock).toBeTruthy();
    });
});
describe('Application is loaded', ()=>{
    it('app is defined', ()=>{
        var _app_ = angular.module('app');
        expect(_app_).toBeTruthy();
    })
});
beforeEach(()=>{
    //mocks setup in $injector from beginning.
    $injector = angular.injector(['ng','app','ngMock']);
    $controller = $injector.get('$controller');
    $rootScope = $injector.get('$rootScope');
})
describe('dev_code_test',()=>{
    it('constant should inject', ()=>{
        //loading actual ng-Constant service.
        $injector.invoke(['TIMEZONE', function (_tz_) {
            expect(_tz_).toBe('Eastern');
        }]);
    });
    it('reverse filter should work', () => {
        //loading actual ng-filter.
        $injector.invoke(['$filter', function(_filter_){
            _reverse_ = _filter_('reverse');
            expect(_reverse_('abcdddd')).toBe('ddddcba');
        }])
    });

    //mock tests: mock $interval:
    it('controller', ()=>{
        //somehow let CtrlA load mocked $interval instead of original $interval?
        //we can't use angular.mock.module, nor  angular.mock.inject
        var _scope_ = {
        };
        var _CtrlA_ = $controller('CtrlA', {
            "$scope": _scope_
        });
        var _interval_ = $injector.get('$interval');
        expect(_scope_.moment).toBe(1234);
        _interval_.flush(2000);
        expect(_scope_.moment).toBe(1234+2);
    });
});